#include <omp.h>
#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

using namespace std;

void TaskOne() {
#pragma omp parallel num_threads(8)
	{
		printf("Hello from one of your threads, my sir! My identifier = %d, I'm one of %d threads. Hello World! \n",
			omp_get_thread_num(), omp_get_num_threads());
	}
}

void TaskTwo() {
	int ThreadsAmount;
	cout << "Enter amount of threads.\n";
	cin >> ThreadsAmount;
	omp_set_num_threads(ThreadsAmount);
#pragma omp parallel if(ThreadsAmount > 1) 
	{
		printf("Thread num = %d, Amount of threads = %d.\n", omp_get_thread_num(), omp_get_num_threads());
	}
}

void TaskThree() {
	int ThreadNum, a = 10, b = 10;
	printf("First paralel part. Before parallel.  a = %d, b = %d \n", a, b);
#pragma omp parallel num_threads(2) private(a) firstprivate(b)
	{
		ThreadNum = omp_get_thread_num();
		a = 10;
		b = 10;
		printf("First paralel part. In parallel.      a = %d, b = %d \n", a, b);
		a += ThreadNum;
		b += ThreadNum;
		printf("First paralel part. In parallel 2.    a = %d, b = %d \n", a, b);
	}
	printf("First paralel part. After parallel.   a = %d, b = %d \n", a, b);

	printf("Second paralel part. Before parallel. a = %d, b = %d \n", a, b);
#pragma omp parallel num_threads(4) shared(a) private(b)
	{
		ThreadNum = omp_get_thread_num();
		b = 10;
		printf("Second paralel part. In parallel.     a = %d, b = %d \n", a, b);
		a -= ThreadNum;
		b -= ThreadNum;
		printf("Second paralel part. In parallel 2.   a = %d, b = %d \n", a, b);
	}
	printf("Second paralel part. After parallel.  a = %d, b = %d \n", a, b);
}

void TaskFour() {
	int a[10], b[10], min = 0, max = 0;
	for (int i = 0; i < 10; i++) {
		a[i] = rand() % 1000;
		printf("a[%d] = %3d ", i, a[i]);
		b[i] = rand() % 1000;
		printf("b[%d] = %3d\n", i, b[i]);
	}

	omp_set_num_threads(2);

#pragma omp parallel if(true) 
	if (omp_get_thread_num() == 0)
	{
		min = *min_element(a, a + 10);
	}
	else if (omp_get_thread_num() == 1)
	{
		max = *max_element(b, b + 10);
	}

	printf("Min in a[] = %d. Max in b[] = %d.\n", min, max);
}

void TaskFive() {
	int d[6][8];
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 8; j++) {
			d[i][j] = rand() % 1000;
			printf("d[%d][%d] = %3d ", i + 1, j + 1, d[i][j]);
		}
		printf("\n");
	}
#pragma omp parallel num_threads(3)
	{
#pragma omp sections 
		{
#pragma omp section
			{
				int mid = 0;
				for (int i = 0; i < 6; i++) {
					for (int j = 0; j < 8; j++) {
						mid += d[i][j];
					}
				}
				mid = mid / 48;
				printf("First section. Thread %d of %d. Average: %d.\n", omp_get_thread_num(), omp_get_num_threads(), mid);
			}
#pragma omp section
			{
				int min = 1000, max = 0, tempMin[6], tempMax[6];
				for (int i = 0; i < 6; i++) {
					tempMin[i] = *min_element(d[i], d[i] + 8);
					tempMax[i] = *max_element(d[i], d[i] + 8);
				}
				min = *min_element(tempMin, tempMin + 6);
				max = *max_element(tempMax, tempMax + 6);
				printf("Second section. Thread %d of %d. Min|Max element: %d | %d.\n", omp_get_thread_num(), omp_get_num_threads(), min, max);
			}
#pragma omp section
			{
				int k = 0;
				for (int i = 0; i < 6; i++) {
					for (int j = 0; j < 8; j++) {
						if (d[i][j] % 3 == 0) k++;
					}
				}
				printf("Third section. Thread %d of %d. The number of multiples of 3: %d.\n", omp_get_thread_num(), omp_get_num_threads(), k);
			}
		}
	}
}

void TaskSix() {
	int a[10], b[10], ka = 0, kb = 0;
	for (int i = 0; i < 10; i++) {
		a[i] = rand() % 1000;
		printf("a[%d] = %3d ", i, a[i]);
		b[i] = rand() % 1000;
		printf("b[%d] = %3d\n", i, b[i]);
	}
#pragma omp parallel for reduction(+ : ka, kb)
	for (int i = 0; i < 10; i++) {
		ka += a[i];
		kb += b[i];
	}
	printf("%d %d \n", ka, kb);
}

void TaskSeven() {
	int a[12], b[12], c[12];
	omp_set_num_threads(3);
#pragma omp parallel for schedule(static, 2)
	for (int i = 0; i < 12; i++) {
		printf("%d %d ", omp_get_thread_num(), omp_get_num_threads());
		a[i] = rand() % 1000;
		printf("a[%d] = %3d ", i, a[i]);
		b[i] = rand() % 1000;
		printf("b[%d] = %3d\n", i, b[i]);
	}
	omp_set_num_threads(4);
#pragma omp parallel for schedule(dynamic, 2)
	for (int i = 0; i < 12; i++) {
		printf("%d %d ", omp_get_thread_num(), omp_get_num_threads());
		c[i] = a[i] + b[i];
		printf("c[%d] = %3d \n", i, c[i]);
	}
}

void TaskEight() {

	double start, stop, time[25];
	int a[32000], b[32000], size = 32000, sectionNum, attemptsAmount;
	for (int i = 0; i < size; i++) {
		a[i] = i;
	}
	for (int i = 0; i < 25; i++) {
		time[i] = 0;
	}
	attemptsAmount = 10000;
	for (int i = 0; i < attemptsAmount; i++) {

		omp_set_num_threads(8);
		start = omp_get_wtime();


		//##########################_Section �1
		sectionNum = 0;
		start = omp_get_wtime();
#pragma omp parallel for
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		

		//##########################_Section �2
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(static)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �3
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(static, 2)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �4
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(static, 8)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �5
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 1)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �6
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 8)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �7
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 80)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}

		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		//##########################_Section �8
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 250)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �9
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 4000)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �10
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 1)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �11
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 8)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �12
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 80)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}

		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		//##########################_Section �13
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 250)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �14
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 4000)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �15
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(runtime)
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �16
		sectionNum++;
		start = omp_get_wtime();
		for (int i = 1; i < size - 1; i++) {
			b[i] = (a[i - 1] + a[i] + a[i + 1]) / 3;
		}

		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		
	}
	for (int i = 0; i < sectionNum+1; i++) {
		printf("Section %d average time of %d attempts: %f \n", i+1, attemptsAmount, time[i]/attemptsAmount);
	}

}

void TaskNine() {
	double start, stop, time[25];
	int sectionNum, attemptsAmount;
	int const size = 250;
	int matrix[size][size], vector[size], result[size];


	printf("Vector: \n");
	for (int i = 0; i < size; i++) {
		vector[i] = rand() % 10;
		result[i] = 0;
		//printf("%4d", vector[i]);
	}

	printf("\n\n");

	printf("Matrix: \n");
	for (int i = 0; i < size; i++){
		//printf("%d", i);
		for (int j = 0; j < size; j++) {
			matrix[i][j] = rand() % 10;
			//printf("%5d", matrix[i][j]);
		}
		//printf("\n");
	}

	for (int i = 0; i < 25; i++) {
		time[i] = 0;
	}

	attemptsAmount = 1000;

	for (int i = 0; i < attemptsAmount; i++) {

		omp_set_num_threads(8);
		start = omp_get_wtime();


		//##########################_Section �1
		sectionNum = 0;
		start = omp_get_wtime();
#pragma omp parallel for
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �2
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(static)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �3
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(static, 2)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �4
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(static, 8)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �5
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 1)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �6
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 8)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �7
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 80)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}

		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		//##########################_Section �8
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 250)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �9
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 4000)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �10
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 1)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �11
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 8)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;


		//##########################_Section �12
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 80)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}

		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		//##########################_Section �13
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 250)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �14
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided, 4000)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �15
		sectionNum++;
		start = omp_get_wtime();
#pragma omp parallel for schedule(runtime)
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;

		//##########################_Section �16
		sectionNum++;
		start = omp_get_wtime();
		for (int i = 0; i < size; i++) {
			result[i] = 0;
			for (int j = 0; j < size; j++) {
				result[i] += matrix[i][j] * vector[j];
			}
		}

		time[sectionNum] += (omp_get_wtime() - start) * 10000;
	}
	for (int i = 0; i < sectionNum + 1; i++) {
		printf("Section %d average time of %d attempts: %f \n", i + 1, attemptsAmount, time[i] / attemptsAmount);
	}
}

void TaskTen() {
	double start, time[25];
	int sectionNum, attemptsAmount, minimum = 1000000, maximum = -1, localMin, localMax;
	int const sizei = 100, sizej = 100; //should be 6 and 8
	int d[sizei][sizej];
	for (int i = 0; i < sizei; i++) {
		for (int j = 0; j < sizej; j++) {
			d[i][j] = rand() % 1000;
			//printf("d[%d][%d] = %3d ", i + 1, j + 1, d[i][j]);
		}
		//printf("\n");
	}

	for (int i = 0; i < 25; i++) {
		time[i] = 0;
	}

	attemptsAmount = 10000;

	for (int i = 0; i < attemptsAmount; i++) {

		//##########################_Section �1 Parallel
		sectionNum = 0;
		start = omp_get_wtime();
#pragma omp parallel for schedule(guided) shared(minimum, maximum) private(localMin, localMax)
		for (int i = 0; i < sizei; i++) {
			localMin = *min_element(d[i], d[i] + sizej);
			localMax = *max_element(d[i], d[i] + sizej);
#pragma omp critical
			{
				if (localMin < minimum) {
					minimum = localMin;
				}
				if (localMax > maximum) {
					maximum = localMax;
				}
			}
		}
		time[sectionNum] += (omp_get_wtime() - start) * 10000;
		sectionNum++;

		//##########################_Section �2 linear
		start = omp_get_wtime();
		int min = 1000, max = 0, tempMin[sizei], tempMax[sizei];
		for (int i = 0; i < sizei; i++) {
			tempMin[i] = *min_element(d[i], d[i] + sizej);
			tempMax[i] = *max_element(d[i], d[i] + sizej);
		}
		min = *min_element(tempMin, tempMin + sizei);
		max = *max_element(tempMax, tempMax + sizei);
		time[sectionNum] += (omp_get_wtime() - start) * 10000;
	}
	printf("\nMin = %d | Max = %d \n", minimum, maximum);
	for (int i = 0; i < sectionNum + 1; i++) {
		printf("Section %d average time of %d attempts: %f \n", i + 1, attemptsAmount, time[i] / attemptsAmount);
	}
}

void TaskEleven() {
	int const size = 30;
	int a[size], count = 0;
	for (int i = 0; i < size; i++) {
		a[i] = rand() % 10;
		if (i % 10 == 0 && i != 0) printf("\n");
		printf("%5d", a[i]);		
	}
	printf("\n");
#pragma omp parallel for 
	for (int i = 0; i < size; i++) {
		if (a[i] % 9 == 0) {
#pragma omp atomic
				count++;
		}
	}
	printf("Count: %d\n", count);
}

void TaskTwelve() {
	int const size = 30;
	int a[size], localMultiple = -1, multiple = -1;
	for (int i = 0; i < size; i++) {
		a[i] = rand() % 10000;
		if (i % 10 == 0 && i != 0) printf("\n");
		printf("%6d", a[i]);
	}
		printf("\n");
#pragma omp parallel for firstprivate(localMultiple)
	for (int i = 0; i < size; i++) {
		if (a[i] % 7 == 0)
		{
			localMultiple = a[i];
		}
#pragma omp critical
		{
			if (localMultiple > multiple) {
				multiple = localMultiple;
			}
		}
	}
	printf("%d\n", multiple);
}

void TaskThirteen() {
	int threadsAmount = 8; int th = threadsAmount-1;
#pragma omp parallel num_threads(threadsAmount)
	{
		for (int i = 0; i < threadsAmount; i++) {
#pragma omp barrier
			//printf(" %d | %d", omp_get_thread_num(), th);
			if (omp_get_thread_num() == th) {
				th--;
				printf("Hello from one of your threads, my sir! My identifier = %d, I'm one of %d threads. Hello World! \n",
					omp_get_thread_num() + 1, omp_get_num_threads());
			}
		}
	}

	printf("###############################################\n");
	/*for (int i = 0; i < threadsAmount; i++) {
#pragma omp parallel if (omp_get_thread_num() == th) num_threads(8)
		//printf(" %d | %d", omp_get_thread_num(), th);			
		th--;
		printf("Hello from one of your threads, my sir! My identifier = %d, I'm one of %d threads. Hello World! \n",
			omp_get_thread_num() + 1, omp_get_num_threads());
	}*/
	th = threadsAmount - 1;
#pragma omp parallel for ordered
	for (int i = threadsAmount; i > 0; i--)
	{
#pragma omp ordered
		//printf("Hello from one of your threads, my sir! My identifier = %d, I'm one of %d threads. Hello World! \n",
		//	omp_get_thread_num() + 1, omp_get_num_threads());
		if (omp_get_thread_num() == i) {
			th--;
			printf("Hello from one of your threads, my sir! My identifier = %d, I'm one of %d threads. Hello World! \n",
				omp_get_thread_num() + 1, omp_get_num_threads());
		}
	}
}

bool ExecuteTask(int taskNum) {

	switch (taskNum) {
		case (1):
			TaskOne();
			break;
		case (2):
			TaskTwo();
			break;
		case (3):
			TaskThree();
			break;
		case (4):
			TaskFour();
			break;
		case (5):
			TaskFive();
			break;
		case (6):
			TaskSix();
			break;
		case (7):
			TaskSeven();
			break;
		case (8):
			TaskEight();
			break;
		case (9):
			TaskNine();
			break;
		case (10):
			TaskTen();
			break;
		case (11):
			TaskEleven();
			break;
		case (12):
			TaskTwelve();
			break;
		case (13):
			TaskThirteen();
			break;
		case (0):
			return false;
		default:
			printf("Wrong task number!");
	}

	return true;
}

void main() {
	bool executing = true;
	int taskNumber;
	printf("Enter task number. (0 for exit) \n");
	while (executing) {
		cin >> taskNumber;
		executing = ExecuteTask(taskNumber);
	}
}
